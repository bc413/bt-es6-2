let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];



let hienThiDanhSachKinh = (arr) =>{
    let contentHtml = "";
    arr.forEach((item) =>{
        let contentTr = `
        <div class="button col-4 ${item.id}">
            <img style="width:100px;" src="${item.src}" alt=""/>
        </div>
        `
        contentHtml += contentTr;
    })
    document.querySelector("#vglassesList").innerHTML = contentHtml
}

hienThiDanhSachKinh(dataGlasses);


let thuKinh = () =>{
    let click = Array.prototype.slice.call(
        document.querySelectorAll(".button")
    );
    click.forEach((item) => {
        item.addEventListener("click", (e)=>{
            e.preventDefault();
            let id = item.classList[2]
            let index = dataGlasses.findIndex((item)=>{
                return item.id == id
            })
            let img = dataGlasses[index].virtualImg;
            let name = dataGlasses[index].name;
            let brand = dataGlasses[index].brand;
            let color = dataGlasses[index].color;
            let price = dataGlasses[index].price;
            let des = dataGlasses[index].description;
            let contentTr = `
            <div id="image" class="item">
                <img style="width:270px;" src="${img}" alt="" />
            </div>
            `
            document.querySelector("#image").innerHTML = contentTr;
            let contentText = `
                <h3 style="font-size: 20px;" >${name} - ${brand} (${color}) </h3>
                <div class="price">
                <span 
                    style="background-color: red; 
                    padding: 5px;
                    border-radius: 5px;
                    margin: 10px 15px 10px 0px;
                    display: inline-block;
                    ">$${price}
                </span>
                <span style="color: green">Stocking</span>
                <p>${des}</p>
                </div>
            `
            document.querySelector("#glassesInfo").innerHTML = contentText
            document.querySelector("#glassesInfo").style.display = "block"
        })
    })
        document.querySelector("#avatar").style.position = "relative";
        document.querySelector("#image").style.position = "absolute";
        document.querySelector("#image").style.top = "37%";
        document.querySelector("#image").style.left = "50%";
        document.querySelector("#image").style.transform = "translate(-50%, -37%)"
};

thuKinh()

let beforeAfter = () =>{
    let before = document.querySelector(".before");
    before.addEventListener("click", () =>{
        document.querySelector(".item").style.display = "none";
    })
    
    let after = document.querySelector(".after");
    after.addEventListener("click",() =>{
        document.querySelector(".item").style.display = "block";
    })
}
beforeAfter()